# Text Analysis Workshop 2022

Contact: qixuan.yang [at] yale.edu

## Introduction

You can find the workshop materials for quantitative text data analysis. Following files are in this repository:

- `2022_workshop_slide.pdf`: Slides for the workshop. If the screenshots from other papers and blogposts are used, the references are also provided.

- `2022_yale_textanalysis_workshop_final.pdf`: Syllabus for the workshop with recommended readings.

- `notebooks`: The google colab / jupyter lab notebooks for different sections. The running example is the annotated dataset from the Comparative Agenda Project (https://www.comparativeagendas.net/eu).

    - `text_as_data_p1_1_handout.ipynb`: Bag of words, pattern matching, basics of spaCy usage, finding entities / facts from texts.

    - `text_as_data_p1_2_handout.ipynb`: Text preprocessing based on bag of words (lemmatization, ngrams, stopword removal etc.), vectorizer for text data, (Optional: text ranking).

    - `text_as_data_p1_3_handout.ipynb`: Clustering of texts through topic modeling, visualization of topic models, text classification, interpretation of feature weights from the text classification, (Optional: quantify uncertainties from text classification models).

    - `text_as_data_p2_1_handout.ipynb`: Basics of word and sentence embeddings based on https://fasttext.cc, clustering based on word embeddings

    - `text_as_data_p2_2_handout.ipynb`: Introduction to transformers and BERT

I recommend you to upload these files to google colab and run it in this managed environment. For Yale affiliates, you can as well click on the following links:

- [Colab: text_as_data_p1_1_handout](https://colab.research.google.com/drive/1cHRB6f_r01SotY1XR1W-P1KcbFqnnLud?usp=share_link)

- [Colab: text_as_data_p1_2_handout](https://colab.research.google.com/drive/1tsZiFW--lErmVX92XItgM1yE81VdL7dJ?usp=share_link)

- [Colab: text_as_data_p1_3_handout](https://colab.research.google.com/drive/1S79JIu-skX2gX3tI5Lsi1lj5UKg8eWz0?usp=share_link)

- [Colab: text_as_data_p2_1_handout](https://colab.research.google.com/drive/1N1ELErPCJFKpr1lKrVhQIC-ezehw7hxi?usp=sharing)

- [Colab: text_as_data_p2_2_handout](https://colab.research.google.com/drive/11J4n1qAqgdIfx1D7k5WmShrcq7RwQszx?usp=share_link)

## Remarks

This workshop was designed for two 2-hour sessions. Therefore, I skipped the mathematical and statistical foundations for Natural Language Processing but gave the intuitions behind the selected methods. The part 2.2 about BERT could not be elaborated due to the time constraints.

I used python and the popular packages in python in practice. I acknowledge that there are also great packages in `R` - choosing python as the programming language is largely for the sake of convenience.

When teaching the workshop, I highlight the importance of qualitative aspect regarding the selection and the evaluation of the methods. I consider many methods as explorative: Based on large chunk of the data, one can get some general patterns for further qualitative analysis in typical documents or abnormal documents. This is especially true when text clustering is used.